package com.imit;

import java.util.Objects;

public class Payment {
    private String name;
    private int payDay;
    private int payMonth;
    private int payYear;
    private int amount;

    public Payment(String name, int payDay, int payMonth, int payYear, int amount) {
        if (payDay > 31 || payDay <= 0) throw new IllegalArgumentException();
        if (payDay > 29 && payMonth == 2 && payYear % 4 == 0) throw new IllegalArgumentException();
        if (payDay > 28 && payMonth == 2 && payYear % 4 != 0) throw new IllegalArgumentException();
        if (payYear < 0) throw new IllegalArgumentException();
        if ((payMonth == 4 || payMonth == 6 || payMonth == 9 || payMonth == 11) && payDay > 30)
            throw new IllegalArgumentException();
        this.name = name;
        this.payDay = payDay;
        this.payMonth = payMonth;
        this.payYear = payYear;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayDay() {
        return payDay;
    }

    public void setPayDay(int payDay) {
        this.payDay = payDay;
    }

    public int getPayMonth() {
        return payMonth;
    }

    public void setPayMonth(int payMonth) {
        this.payMonth = payMonth;
    }

    public int getPayYear() {
        return payYear;
    }

    public void setPayYear(int payYear) {
        this.payYear = payYear;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return String.format("Плательщик: «%s» дата: %d.%d.%d сумма: %d руб. %d коп.", name, payDay, payMonth, payYear, amount / 100, amount % 100);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return payDay == payment.payDay &&
                payMonth == payment.payMonth &&
                payYear == payment.payYear &&
                amount == payment.amount &&
                Objects.equals(name, payment.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, payDay, payMonth, payYear, amount);
    }
}
