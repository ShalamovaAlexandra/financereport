package com.imit;

import com.google.gson.Gson;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class FinanceReport {
    private Map<Integer, Payment> payMap = new TreeMap<>();
    private Integer num = 1;

    public FinanceReport() {

    }

    public FinanceReport(FinanceReport report) {
        for (int i = 1; i <= report.getPayMap().size(); i++) {
            payMap.put(i, new Payment(report.getPayMap().get(i).getName(), report.getPayMap().get(i).getPayDay(),
                    report.getPayMap().get(i).getPayMonth(), report.getPayMap().get(i).getPayYear(), report.getPayMap().get(i).getAmount()));
        }
        num = report.getNum();
    }

    public int addPayment(Payment pay) {
        payMap.put(num, pay);
        this.num += 1;
        return num - 1;
    }

    public void deletePayment(int id) {
        payMap.remove(id);
    }

    public Payment getPayment(int id) {
        return payMap.get(id);
    }

    public void changePayment(Payment pay, int id) {
        payMap.put(id, pay);
    }

    public Integer getNum() {
        return num;
    }

    public Map<Integer, Payment> getPayMap() {
        return payMap;
    }

    public List<Payment> getPaymentsLessThan(int amount) {
        List<Payment> payList = new LinkedList<>();
        for (Payment pay : payMap.values()) {
            if (pay.getAmount() < amount) {
                payList.add(pay);
            }
        }
        return payList;
    }

    public Integer gotDailyAmount(String date) {
        try {
            String dd = date.substring(0, 2);
            String mm = date.substring(3, 5);
            String yy = date.substring(6, 8); //compare to payment date%100
            Integer idd = Integer.parseInt(dd);
            Integer imm = Integer.parseInt(mm);
            Integer iyy = Integer.parseInt(yy);
            Integer sum = 0;
            for (Payment pm : payMap.values()) {
                if (pm.getPayDay() == idd && pm.getPayMonth() == imm && pm.getPayYear() % 100 == iyy) {
                    sum += pm.getAmount();
                }
            }
            return sum;
        } catch (IndexOutOfBoundsException | NumberFormatException ex) {
            System.out.println("error");
            return null;
        }
    }

    public List<String> monthNoAmoutThisYear(int year) {
        String[] monthNameArr = {
                "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
        };
        Map<Integer, Integer> monthMap = new TreeMap<>();
        List<String> monthList = new LinkedList<>();
        for (int i = 1; i <= 12; i++) {
            monthMap.put(i, 0);
        }
        for (Payment pay : payMap.values()) {
            if (pay.getPayYear() == year) {
                monthMap.put(pay.getPayMonth(), 1);
            }
        }
        for (int i = 1; i <= 12; i++) {
            if (monthMap.get(i) == 0) monthList.add(monthNameArr[i - 1]);
        }
        return monthList;
    }

    public void serializeTraineeToBinaryFile(File file) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            Gson gson = new Gson();
            gson.toJson(this, bw);
        }
    }

    public void deserializeTraineeFromBinaryFile(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            Gson gson = new Gson();
            FinanceReport financeReport = gson.fromJson(br, FinanceReport.class);
            this.payMap = financeReport.payMap;
        }
    }


}
