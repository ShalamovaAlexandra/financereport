package com.imit;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class testFinanceReport {
    @Test
    public void testGetPaymentsLessThan() {
        //PRE TEST
        Payment[] pmArr = {new Payment("Вася Пупкин", 12, 10, 2018, 1500),
                new Payment("Анна Олол", 12, 10, 2018, 1000),
                new Payment("Кирилл Пупов", 12, 1, 2018, 700),
                new Payment("Кирилл Пупов", 13, 2, 2018, 400),
                new Payment("Кирилл Пупов", 30, 3, 2018, 100),
                new Payment("Кирилл Пупов", 15, 4, 2018, 200),
                new Payment("Кирилл Пупов", 24, 5, 2018, 3000),
                new Payment("Кирилл Пупов", 24, 11, 2018, 8800),
                new Payment("Кирилл Пупов", 24, 11, 2018, 8800)
        };
        FinanceReport finReport = new FinanceReport();
        for (Payment pm : pmArr) {
            finReport.addPayment(pm);
        }
        //MAIN TEST
        assertEquals(finReport.getPaymentsLessThan(200).size(), 1);
        assertEquals(finReport.getPaymentsLessThan(10000).size(), pmArr.length);
        assertEquals(finReport.getPaymentsLessThan(700).size(), 3);
        assertEquals(finReport.getPaymentsLessThan(1500).size(), 5);
    }

    @Test
    public void testDailyAmount() {
        //PRE TEST
        Payment[] pmArr = {new Payment("Вася Пупкин", 12, 10, 2018, 1500),
                new Payment("Анна Олол", 12, 10, 2018, 1000),
                new Payment("Анна Хех", 12, 10, 2018, 100),
                new Payment("Кирилл Пупов", 12, 1, 2018, 700),
                new Payment("Кирилл Пупов", 13, 2, 2018, 400),
                new Payment("Кирилл Пупов", 30, 3, 2016, 100),
                new Payment("Кирилл Пупов", 15, 4, 2018, 200),
                new Payment("Кирилл Пупов", 24, 5, 1997, 3000),
                new Payment("Кирилл Пупов", 24, 11, 2018, 8800)
        };
        FinanceReport finReport = new FinanceReport();
        for (Payment pm : pmArr) {
            finReport.addPayment(pm);
        }
        //MAIN TEST
        int ans = finReport.gotDailyAmount("12.10.18");
        assertEquals(ans, 2600);
        ans = finReport.gotDailyAmount("30.03.16");
        assertEquals(ans, 100);
        ans = finReport.gotDailyAmount("24.05.97");
        assertEquals(ans, 3000);
    }

    @Test
    public void testMonthNoAmoutThisYear() {
        //PRE TEST
        Payment[] pmArr = {new Payment("Вася Пупкин", 12, 12, 2018, 1500),
                new Payment("Вася Пупкин", 12, 12, 2016, 1500),
                new Payment("Вася Пупкин", 12, 10, 2016, 1500),
                new Payment("Вася Пупкин", 12, 10, 2014, 1500),
                new Payment("Вася Пупкин", 12, 9, 2014, 1500),
                new Payment("Вася Пупкин", 12, 10, 2015, 1500),
                new Payment("Анна Олол", 12, 10, 2018, 1000),
                new Payment("Кирилл Пупов", 12, 1, 2018, 700),
                new Payment("Кирилл Пупов", 13, 2, 2018, 400),
                new Payment("Кирилл Пупов", 30, 3, 2018, 100),
                new Payment("Кирилл Пупов", 15, 4, 2018, 200),
                new Payment("Кирилл Пупов", 24, 5, 2018, 3000),
                new Payment("Кирилл Пупов", 24, 11, 2018, 8800)
        };
        FinanceReport finReport = new FinanceReport();
        for (Payment pm : pmArr) {
            finReport.addPayment(pm);
        }
        //MAIN TEST
        assertEquals(finReport.monthNoAmoutThisYear(2018).size(), 4);
        assertEquals(finReport.monthNoAmoutThisYear(2016).size(), 10);
        assertEquals(finReport.monthNoAmoutThisYear(2015).size(), 11);
        assertEquals(finReport.monthNoAmoutThisYear(2014).size(), 10);

        for (String str : finReport.monthNoAmoutThisYear(2018)) {
            System.out.println(str);
        }
        System.out.println(finReport.toString());

    }

    @Test
    public void testSerialize() throws IOException, ClassNotFoundException {
        //PRE TEST
        Payment[] pmArr = {new Payment("Вася Пупкин", 12, 12, 2018, 1500),
                new Payment("Вася Пупкин", 12, 12, 2016, 1500),
                new Payment("Вася Пупкин", 12, 10, 2016, 1500),
                new Payment("Вася Пупкин", 12, 10, 2014, 1500),
                new Payment("Вася Пупкин", 12, 9, 2014, 1500),
                new Payment("Вася Пупкин", 12, 10, 2015, 1500),
                new Payment("Анна Олол", 12, 10, 2018, 1000),
                new Payment("Кирилл Пупов", 12, 1, 2018, 700),
                new Payment("Кирилл Пупов", 13, 2, 2018, 400),
                new Payment("Кирилл Пупов", 30, 3, 2018, 100),
                new Payment("Кирилл Пупов", 15, 4, 2018, 200),
                new Payment("Кирилл Пупов", 24, 5, 2018, 3000),
                new Payment("Кирилл Пупов", 24, 11, 2018, 8800)
        };
        FinanceReport finReport = new FinanceReport();
        for (Payment pm : pmArr) {
            finReport.addPayment(pm);
        }
        //MAIN TEST

        File file = new File("1");
        finReport.serializeTraineeToBinaryFile(file);

        FinanceReport newReport = new FinanceReport();
        newReport.deserializeTraineeFromBinaryFile(file);
        assertEquals(finReport.getPayMap().size(), newReport.getPayMap().size());
    }

    @Test
    public void testCopy() {
        Payment pm1 = new Payment("Вася Пупкин", 12, 12, 2018, 1500);
        Payment pm2 = new Payment("Вася Пупкин", 12, 12, 2016, 1500);

        FinanceReport finReport = new FinanceReport();
        finReport.addPayment(pm1);
        finReport.addPayment(pm2);
        FinanceReport newReport = new FinanceReport(finReport);
        finReport.addPayment(new Payment("Кирилл Пупов", 24, 11, 2018, 8800));

        assertEquals(newReport.getPayMap().size(), 2);

        assertEquals(finReport.getPayMap().size(), 3);
        pm1.setAmount(100);
        assertEquals(newReport.getPayment(1).getAmount(),1500);
    }
}
